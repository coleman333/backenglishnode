var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var session = require('express-session');
var passport = require('passport');
var passportConfig = require('./config/passportConfig');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/EnglishDB');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!");
});

passportConfig(passport);

var users = require('./routes/users');
var auth = require('./routes/auth');
var profile = require('./routes/profile');
var vocabulary = require('./routes/vocabulary');
var tenses = require('./routes/tence');
var InsertNewTenseSentence = require('./routes/InsertNewTenseSentence');

var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors({
    origin: true,
    credentials: true
}));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false}
}));
app.use(passport.initialize());
app.use(passport.session());
//app.get('/test', function (req, res, next) {
// console.log('hello from test');
//res.json(
//    {
//        message:"the word had written"
//    });
// res.end('hello!');
//});
app.use('/auth', auth);
app.get('/setup', function (req, res, next) {
    var Picture = require('./models/Picture');

    //Picture.create({
    //    englishWord: 'world',
    //    rusWord: 'мир',
    //    theme:'theme',
    //    picture:'http://localhost:3000/images/gomer.jpg'
    //},function(err){
    //
    //});
    //
    //Picture.create({
    //    englishWord: 'bed',
    //    rusWord: 'кровать',
    //    theme: 'theme',
    //    picture:'http://localhost:3000/images/sqirrel.jpg'
    //}, function (err) {
    //
    //});
    //Picture.create({
    //    englishWord: 'cat',
    //    rusWord: 'кошка',
    //    theme: 'theme',
    //    picture:'http://localhost:3000/images/cat.jpg'
    //}, function (err) {
    //
    //});
    //Picture.create({
    //    englishWord: 'pen',
    //    rusWord: 'карандаш',
    //    theme: 'theme',
    //    picture:'http://localhost:3000/images/dog.jpg'
    //}, function (err) {
    //
    //});
    //Picture.create({
    //    englishWord: 'batman',
    //    rusWord: 'бетмен',
    //    theme: 'theme',
    //    picture:'http://localhost:3000/images/cat.jpg'
    //}, function (err) {
    //
    //});
    //Picture.create({
    //    englishWord: 'comp',
    //    rusWord: 'комп',
    //    theme: 'theme',
    //    picture:'http://localhost:3000/images/dog.jpg'
    //}, function (err) {
    //
    //});

    require('./models/User').update({_id: '571b94a99a239da23d3ea38b'}, {$set: {progressEng: ['571cd97e6501bd3c37be06e5', '571cd97e6501bd3c37be06e6']}}).exec();

    res.end('done');


});
app.get('/setup-sentence', function (req, res, next) {
    var Sentence = require('./models/Sentencce');

    //Sentence.create({
    //    sentenceRus: 'бетмен насрал в штаны',
    //    sentence: ['Batman ', 'shitted', 'in', 'trousers'],
    //    theme: 'theme'
    //}, function (err) {
    //
    //});
    //Sentence.create({
    //    sentenceRus: 'у меня есть жирный кот',
    //    sentence: ['I', 'have', 'the','fat', 'cat' ],
    //    theme: 'theme'
    //}, function (err) {
    //});
    //Sentence.create({
    //sentenceRus:'Мама мыла раму папу',
    //sentence:['The','mother','washed','framed','father'],
    //theme:'theme'
    //},function (err){});
    //
    //Sentence.create({
    //   sentenceRus:'маленький мальчик с пулеметом играл',
    //    sentence:['Little','boy','played','with','the','mashinegun'],
    //    theme:'theme'
    //},function (err){}) ;
    //
    //Sentence.create({
    //sentenceRus:'мир , труд ,май',
    //    sentence:['world','labor','march'],
    //    theme:'theme'
    //},function(err){});

    //Sentence.create({
    //    sentenceRus:'',
    //    sentence:['','','',''],
    //    theme:'theme'
    //},function(err){});

    // Sentence.create({
    //    sentenceRus: '',
    //    sentence: [],
    //    theme:'theme'
    //},function(err){
    //
    //});Sentence.create({
    //    sentenceRus: 'world',
    //    sentence: [],
    //    theme:'theme'
    //},function(err){
    //
    //});Sentence.create({
    //    sentenceRus: 'world',
    //    sentence: [],
    //    theme:'theme'
    //},function(err){
    //
    //});Sentence.create({
    //    sentenceRus: 'world',
    //    sentence: [],
    //    theme:'theme'
    //},function(err){
    //
    //});Sentence.create({
    //    sentenceRus: 'world',
    //    sentence: [],
    //    theme:'theme'
    //},function(err){
    //
    //});Sentence.create({
    //    sentenceRus: 'world',
    //    sentence: [],
    //    theme:'theme'
    //},function(err){
    //
    //});

    res.end('done');


});

app.use('/api/profile', isAuthenticated, profile);
app.use('/api/users', users);
app.use('/api/vocabulary', isAuthenticated, vocabulary);
app.use('/tenses', isAuthenticated, tenses);
app.use('/add-new-tense-sentence', isAuthenticated, InsertNewTenseSentence);

// var Tense = require('./models/Tences');
// app.get('/tenses/setup', function (req, res, next) {
//     Tense.create({
//         SentenceEng: 'asd2',
//         SentenceRus: 'zxc2'
//     }, function (error, tense) {
//         if (error) {
//             console.error(error);
//             return next(error);
//         }
//         res.json({message: 'success', tense: tense});
//     });
// });
// app.get('/focus', isAuthenticated, function (req, res, next) {
//     Tense.findById('5794dd36f712067f50a498c8', function (err, tense) {
//         req.user.progressTenses.push(tense);
//         req.user.save(function () {
//             res.end('done');
//         })
//     })
// });
function isAuthenticated(req, res, next) {

    if (req.isAuthenticated()) {
        return next();
    }
    res.statusCode = 403;
    res.json({
        message: 'Access denied!'
    });
}


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});


module.exports = app;
