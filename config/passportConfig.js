var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/User');

function passportConfig(passport) {

    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    passport.use(new LocalStrategy({
            usernameField: 'email'
        },
        function (email, password, done) {
            User.findOne({email: email}, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {message: 'Incorrect email.'});
                }

                user.comparePassword(password, function (err, isMatch) {
                    if (!isMatch) {
                        return done(null, false, {message: 'Incorrect password.'});
                    }
                    return done(null, user);
                });
            });
        }
    ));
}

module.exports = passportConfig;