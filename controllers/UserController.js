var User = require('../models/User');
var Picture = require('../models/Picture');

var passport = require('passport');
var omit = require('lodash/omit');

module.exports.create = function (req, res, next) {

    var user = new User(req.body);
    user.save(function (err) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.json({
            message: 'User created successful!'
        })
    });
};

module.exports.update = function (req, res, next) {


};

module.exports.find = function (req, res, next) {


};

module.exports.delete = function (req, res, next) {


};

module.exports.findOne = function (req, res, next) {


};


module.exports.getUserProfile = function (req, res, next) {

    var user = omit(req.user, 'password');

    if (user.pageNumber) {
        Picture.findById(user.pageNumber)
            .exec(function (err, picture) {
                if (err) {
                    console.error(err);
                    return next(err);
                }
                if (picture) {
                    user.pageNumber = picture;
                }
            });
    }
    res.json(user);
};

module.exports.login = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            console.error(err);
            return next(err);
        }
        if (user) {
            req.logIn(user, function (err) {
                if (err) {
                    console.error(err);
                    return next(err);
                }
                res.json({
                    id: user._id,
                    message: 'You have access!'
                });
            });
        } else {
            res.statusCode = 403;
            res.json({
                message: info.message
            });
        }
    })(req, res, next);

};
module.exports.logout = function (req, res, next) {
    req.logOut();
    res.end();
};
