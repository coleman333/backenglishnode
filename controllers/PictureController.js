var Picture = require('../models/Picture');
var differenceWith = require('lodash/differenceWith');
var sample = require('lodash/sample');
var sampleSize = require('lodash/sampleSize');
var remove = require('lodash/remove');
var shuffle = require('lodash/shuffle');


module.exports.getWords = function (req, res, next) {
    var theme = req.params.theme;
    var lang = req.params.lang;
    var wordsArray;
    switch (lang) {
        case 'ru':
        {
            wordsArray = 'progressRus';
        }
            break;
        case 'en':
        {
            wordsArray = 'progressEng';
        }
            break;
        default:
            wordsArray = 'progressEng';
    }

    Picture.find({theme: theme}, function (err, pictures) {
        if (err) {
            console.error(err);
            return next(err);
        }
        //pictures
        //req.user.progressEng
        //req.user.progressRus
        if (pictures.length < 1) {
            res.statusCode = 404;
            res.json({
                message: 'words not found in db'
            });
            return;
        }
        var words = differenceWith(pictures, req.user[wordsArray], function (el1, el2) {
            return el1._id.id == el2.id
        });                                         //????

        if (words.length > 0) {
            var word = sample(words);
            remove(pictures, function (elem) {
                return elem._id === word._id;
            });
            var anotherWords = sampleSize(pictures, 4);
            anotherWords.push(word);
            res.json({
                //  theme:Theme,
                mainWord: word,
                anotherWords: shuffle(anotherWords)
            });
        } else {
            res.statusCode = 404;
            res.json({
                message: 'words not found'
            })
        }

    });
};

module.exports.getWord = function (req, res, next) {
    var theme = req.params.theme;

    Picture.find({theme: theme}, function (err, pictures) {
        if (err) {
            console.error(err);
            return next(err);
        }

        if (pictures.length < 1) {
            res.statusCode = 404;
            res.json({
                message: 'words not found in db'
            });
            return;
        }

        var words = differenceWith(pictures, req.user.progressWords, function (el1, el2) {
            return el1._id.id == el2.id
        });

        if (words.length > 0) {
            var word = sample(words);

            res.json({
                mainWord: word,
                shaffledWord: shuffle(word.englishWord)
            });
        } else {
            req.user.progressWords = [];
            req.user.save(function (error) {
                if (error) {
                    console.error(error);
                    return next(error);
                }
                module.exports.getWord(req, res, next);
            });
            // res.statusCode = 404;
            // res.json({
            //      message: 'words not found'
            // })
        }

    });
};

module.exports.getAllWords = function (req, res, next) {
    // var login = res.login();
    Picture.find({}, function (err, pictures) {
        if (err) {
            console.log(err);
            return next(err);
        }
        if (pictures.length < 1) {
            res.statusCode = 404;
            res.json(
                {
                    message: "There are no any words in your DB"
                });
            // return;
        }
        else {
            res.json(
                {
                    //englishWord :pictures[englishWord],
                    //rusWord:pictures.rusWord,
                    //image:pictures.picture,
                    //theme:pictures.theme
                    //audio:pictures.audio
                    pictures: pictures
                })
        }

    });
};
module.exports.insertWords = function (req, res, next) {
    req.user.mark += req.body.mark;
    req.user.progressWords.push(req.body.insert_word);
    req.user.save();
    res.end();
};

//module.exports.getAllWords = function(req,res,next)
//{
//    //req.body.word
//     Picture.create(req.body,function(err){
//         if(err)
//         {
//             console.log(err);
//             return next(err);
//         }
//         else
//         //res.json(
//         //    {
//         //        message:"the word had written"
//         //    });
//         res.end('the word had written');
//     });
//
//
//};
