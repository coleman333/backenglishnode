var Tense = require('../models/Tences.js');
var sample = require('lodash/sample');
var forOwn = require('lodash/forOwn');
var map = require('lodash/map');

module.exports.getSentence = function (req, res, next) {

    //
    // async.waterfall(
    //     [
    //         function (callback) {
    //             Tense.find({}, function (error, tenses) {
    //                 if (error) {
    //                     return callback(error);
    //                 }
    //                 callback(null, tenses);
    //             });
    //         },
    //         function (tenses, callback) {
    //             Tense.find({}, function (error, tenses2) {
    //                 if (error) {
    //                     return callback(error);
    //                 }
    //                 callback(null, {tenses: tenses, tenses2: tenses2})
    //             });
    //         }
    //     ],
    //     function (err, res) {
    //
    //     }
    // );

    Tense.find({_id: {$nin: req.user.progressTenses}}, '_id', function (error, tenses) {
        if (error) {
            console.error(error);
            return next(error);
        }
        var result = sample(tenses);
        Tense.findById(result._id, function (error, tense) {
            if (error) {
                console.error(error);
                return next(error);
            }
            var result = [];

            for (var i = 0; i < tense.blocks.length; i++) {
                for (var j = 0; j < tense.blocks[i].words.length; j++) {
                    result.push({
                        word: tense.blocks[i].words[j].word,
                        block: tense.blocks[i].block+1,
                        isCorrect:tense.blocks[i].words[j].isCorrect
                    })
                }

            }

            var data = {
                sentenceRus: tense.SentenceRus,
                sentenceEng: tense.SentenceEng,
                sentenceArrays: result
            };

            res.json(data);
        });
    });
};

module.exports.insertNewSentence = function (req, res, next) {

    var data = {
        theme: 'theme',
        SentenceEng: req.body.sentenceEng,
        SentenceRus: req.body.sentenceRus,
        blocks: []
    };
    // console.dir(req.body);

    forOwn(req.body, function (value, key) {
        if (isNaN(key)) {
            return;
        }
        var words = map(value, function (elem, key) {
            return {
                word: elem,
                isCorrect: key === 'word1' ? true : false
            }
        });
        var block = key;
        data.blocks.push({words: words, block: block});
    });

    // console.dir(data);
    var sentence = new Tense(data);

    sentence.save(function (err) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.json({
            message: 'Sentence added'
        })
    });
};