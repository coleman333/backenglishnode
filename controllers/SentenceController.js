var Sentence = require('../models/Sentencce');
var shuffle = require('lodash/shuffle');
var sample = require('lodash/sample');
var differenceWith = require('lodash/differenceWith');



module.exports.getSentence = function (req, res, next) {
    var theme = req.params.theme;
    Sentence.find({theme: theme}, function (err, sentences) {
        if (err) {
            console.error(err);
            return next(err);
        }
        if (sentences.length < 1) {
            res.statusCode = 404;
            res.json({
                message: 'Sentences not found in db'
            });
            return;
        }
        var unusedSentences = differenceWith(sentences, req.user.progressSentence, function (el1, el2) {
            return el1._id.id == el2.id
        });

        if (unusedSentences.length > 0) {
            var sentence = sample(unusedSentences);

            res.json({
                sentenceRus: sentence.sentenceRus,
                mainSentence: sentence.sentence,
                shaffledSentence: shuffle(sentence.sentence)
            });
        } else {
            res.statusCode = 404;
            res.json({
                message: 'Sentences not found'
            })
        }

    });
};
