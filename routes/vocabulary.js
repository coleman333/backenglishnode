var express = require('express');
var router = express.Router();
var PictureController = require('../controllers/PictureController');
var SentenceController = require('../controllers/SentenceController');
/* GET users listing. */

router.get('/sentence_constructor/:theme',SentenceController.getSentence);
router.get('/word-constructor/:theme', PictureController.getWord);
router.post('/word-constructor/:theme', PictureController.insertWords);
router.get('/dictionary/', PictureController.getAllWords);
router.get('/:theme/:lang', PictureController.getWords);

module.exports = router;
