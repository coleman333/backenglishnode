var express = require('express');
var router = express.Router();
var UserController = require('../controllers/UserController');

/* GET users listing. */
router.route('/').get(UserController.getUserProfile);


module.exports = router;
