var express = require('express');
var router = express.Router();
var UserController = require('../controllers/UserController');

/* GET users listing. */
router.post('/login', UserController.login);
router.get('/logout', UserController.logout);
router.get('/isauth', function (req, res, next) {
    res.statusCode = req.isAuthenticated() ? 200 : 403;
    res.end();
});

module.exports = router;
