var express = require('express');
var router = express.Router();
var TenceController = require('../controllers/TenceController');

router.route('/').post(TenceController.insertNewSentence);


module.exports = router;