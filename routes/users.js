var express = require('express');
var router = express.Router();
var UserController = require('../controllers/UserController');

/* GET users listing. */
router.route('/')
    .post(UserController.create)
    .get(UserController.find);

router.route('/:id')
    .put(UserController.update)
    .delete(UserController.delete)
    .get(UserController.findOne);

module.exports = router;
