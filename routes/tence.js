var express = require('express');
var router = express.Router();
var TenceController = require('../controllers/TenceController');

router.route('/').get(TenceController.getSentence);


module.exports = router;
