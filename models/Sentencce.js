var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var sentenceSchema = new Schema({
    sentenceRus: {type: String, required: true},
    sentence: [{type: String, required: true}],
    theme: {type: String}
});

module.exports = mongoose.model('Sentence', sentenceSchema);
