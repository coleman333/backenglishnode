var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var pictureSchema = new Schema({
    englishWord: {type: String, required: true},
    rusWord: {type: String, required: true},
    picture: {type: String},
    theme:{type:String}

   // number: {type: Number}
});


module.exports = mongoose.model('Picture', pictureSchema);
