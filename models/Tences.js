var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var tenseSchema = new Schema({
    SentenceEng: {type: String, required: true},
    SentenceRus: {type: String, required: true},
    blocks: [{
        words: [{
            word: {type: String, required: true},
            isCorrect: {type: Boolean, default: false}
        }],
        block: {type: Number, default: 0}
    }],

    theme: {type: String}
});

module.exports = mongoose.model('Tense', tenseSchema);
