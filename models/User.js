var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

var userSchema = new Schema({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    login: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    mark: {type: Number, default: 0},
    progressEng: [{type: Schema.ObjectId, ref: 'Picture'}],
    progressRus: [{type: Schema.ObjectId, ref: 'Picture'}],
    progressWords: [{type: Schema.ObjectId, ref: 'Picture'}],
    progressSentence: [{type: Schema.ObjectId, ref: 'Sentence'}],
    progressTenses: [{type: Schema.ObjectId, ref: 'Tense'}],
    lastDateMarkModified: {type: Date, default: Date.now()}
    /*avatar: {type: String}*/
});


userSchema.pre('save', function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', userSchema);


//multer
//droplet